========
Diagonal
========

A library for FRED service diagnostics, usable for both the client & server side.

Client usage
============

``DiagnosticsClient`` is a low-level client to obtain particular information
from a single server.

.. code-block:: python

    from diagonal import DiagnosticsClient

    async def print_localhost():
        client = DiagnosticsClient("localhost:2240")
        about = await client.about()
        print(f"Server version: {about.server_version}, providing {len(about.api_versions)} APIs.")
        status = await client.status()
        print("Status of services:")
        for service_name, service_info in status.items():
            print(f"- {service_name}: {service_info.status}, note: {service_info.note}")

Use ``collect_server_diagnostics`` to collect diagnostic information
from multiple servers.

.. code-block:: python

    from diagonal import collect_server_diagnostics
    from diagonal.schema import Server

    async def print_summary() -> None:
        servers = [Server(netloc="server1:2240"), Server(netloc="server2:2250")]
        diag = await collect_server_diagnostics(servers)
        print(f"Collected diagnostics from {len(diag.servers)} servers. Got {len(diag.failures)} failures.")
        print(f"Summary status: {diag.summary_status}")

Server usage
============

.. code-block:: python

    from diagonal import DiagnosticsServicer, ServiceStatusInfo
    from fred_api.fileman import service_fileman_grpc_pb2  # the service(s) provided by the server

    def my_service_status_callback() -> ServiceStatusInfo:
        ...  # obtain the service status information

    diag = DiagnosticsServicer(server_version="1.0.0")
    services = diag.add_grpc_api(service_fileman_grpc_pb2)
    for service in services:
        diag.add_service_status(service, my_service_status_callback)
    diag.add_to_server(my_grpc_server)
