from functools import wraps
from typing import Any, Callable, Dict, Iterable, TypeVar, cast

import grpc

from .constants import STATUS_CODE_SEVERITY, StatusCode
from .exceptions import DiagnosticsError, DiagnosticsNotImplemented, ServiceUnavailable

TCallable = TypeVar("TCallable", bound=Callable)

TKey = TypeVar("TKey")
TVal = TypeVar("TVal")


def catch_errors(func: TCallable) -> TCallable:
    @wraps(func)
    async def _inner(*args: Any, **kwargs: Any) -> Any:
        try:
            return await func(*args, **kwargs)
        except grpc.RpcError as error:
            if error.code() == grpc.StatusCode.UNAVAILABLE:
                raise ServiceUnavailable from error
            if error.code() == grpc.StatusCode.UNIMPLEMENTED:
                raise DiagnosticsNotImplemented from error
            raise DiagnosticsError from error
        except Exception as error:
            raise DiagnosticsError from error

    return cast(TCallable, _inner)


def filter_dict(
    data: Dict[TKey, TVal], keys_to_keep: Iterable[TKey]
) -> Dict[TKey, TVal]:
    """Filter dictionary to keep specified keys.

    If `keys_to_keep` is empty, the original dictionary is returned.
    """
    if not keys_to_keep:
        return data
    return {key: value for key, value in data.items() if key in keys_to_keep}


def aggregate_status_code(statuses: Iterable[StatusCode]) -> StatusCode:
    """Return the most severe status of the input statuses."""
    worst_status = STATUS_CODE_SEVERITY[0]
    for st in statuses:
        if STATUS_CODE_SEVERITY.index(st) > STATUS_CODE_SEVERITY.index(worst_status):
            worst_status = st
    return worst_status
