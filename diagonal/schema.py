"""Models of diagnostic data."""

from functools import cached_property
from pathlib import Path
from typing import Dict, List, Optional, Union

import grpc
from fred_types import BaseModel
from pydantic import ConfigDict, computed_field, model_validator

from ._utils import aggregate_status_code
from .constants import ServiceStatusCode, StatusCode


class About(BaseModel):
    """Information about server and API versions."""

    server_version: str
    api_versions: Dict[str, str]


class ServiceStatusInfo(BaseModel):
    """Status information of a service.

    Equivalent to `StatusReply.Data.Service` in the diagnostic gRPC API.
    """

    status: ServiceStatusCode
    note: str = ""
    extras: Dict[str, str] = {}


class Server(BaseModel):
    """Server to get diagnostics from.

    Args:
        netloc: Network location of the gRPC server.
        credentials:
            Credentials for a secure channel connection.
            Mutually exclusive with `ssl_cert`.
            If none of them is provided, an insecure channel is used.
        ssl_cert:
            Path to a file with SSL certificate.
            Mutually exclusive with `credentials`.
        services:
            gRPC services to get diagnostics from.
            If empty, all services of the server are included.
    """

    model_config = ConfigDict(arbitrary_types_allowed=True)

    netloc: str
    credentials: Optional[grpc.ChannelCredentials] = None
    ssl_cert: Union[str, Path, None] = None
    services: List[str] = []

    @model_validator(mode="after")
    def check_credentials(self) -> "Server":
        """Validate that at most one of `credentials` and `ssl_cert` is provided."""
        if self.credentials is not None and self.ssl_cert is not None:
            raise ValueError("Only one of credentials and ssl_cert may be provided.")
        return self


class ServerInfo(BaseModel):
    """Information about the server, provided APIs and services."""

    netloc: str
    server_version: str
    api_versions: Dict[str, str]
    services: Dict[str, ServiceStatusInfo]

    @computed_field  # type: ignore[prop-decorator]
    @cached_property
    def summary_status(self) -> StatusCode:
        """Aggregate status code of all services on the server.

        - If any service has ERROR status, the aggregated status is ERROR.
        - If no service has ERROR status but any of them has a code WARNING or UNKNOWN,
          the aggregated status is WARNING.
        - If all services are OK, then the aggregated status is OK.
        """
        return aggregate_status_code(
            [service.status.to_status_code() for service in self.services.values()]
        )


class ServerFailure(BaseModel):
    """Information about invalid response from a server."""

    netloc: str
    type: str
    message: str


class ServerDiagnostics(BaseModel):
    """Diagnostic information collected from multiple servers.

    Attributes:
        servers:
            Detailed diagnostics for each server we could get the information from.
        failures:
            Failure details for servers that failed to respond.
    """

    servers: List[ServerInfo] = []
    failures: List[ServerFailure] = []

    @computed_field  # type: ignore[prop-decorator]
    @cached_property
    def summary_status(self) -> StatusCode:
        """Aggregate status code of all servers.

        - If any server has the ERROR code or failed to respond,
          the aggregated status is ERROR.
        - If no server has the ERROR code but any server has the WARNING code,
          the aggregated status is WARNING.
        - If all servers are OK, the aggregated status is OK.
        """
        if self.failures:
            return StatusCode.ERROR
        return aggregate_status_code([server.summary_status for server in self.servers])
