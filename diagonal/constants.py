"""Diagonal constants."""

from enum import Enum, unique


@unique
class StatusCode(str, Enum):
    """Generic status code."""

    OK = "ok"
    WARNING = "warning"
    ERROR = "error"


# From the least severe to the most
STATUS_CODE_SEVERITY = (StatusCode.OK, StatusCode.WARNING, StatusCode.ERROR)


@unique
class ServiceStatusCode(str, Enum):
    """Status code of a service.

    Equivalent to `StatusReply.Data.Service.Status` in the diagnostic gRPC API.
    """

    UNKNOWN = "unknown"
    OK = "ok"
    WARNING = "warning"
    ERROR = "error"

    def to_status_code(self) -> StatusCode:
        """Convert to generic status code."""
        return StatusCode(self.value if self.value != "unknown" else "warning")
