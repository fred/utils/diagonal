"""Diagnostic exceptions."""


class DiagnosticsError(Exception):
    """Common base for all diagnostics exceptions."""


class DiagnosticsNotImplemented(DiagnosticsError):
    """Diagnostics service is not implemented."""


class ServiceUnavailable(DiagnosticsError):
    """Service is unavailable."""
