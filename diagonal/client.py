"""Diagnostics client."""

import asyncio
import logging
from typing import Dict, Iterable, cast

from fred_api.diagnostics.service_diagnostics_grpc_pb2 import StatusReply
from fred_api.diagnostics.service_diagnostics_grpc_pb2_grpc import DiagnosticsStub
from frgal import GrpcDecoder
from frgal.aio import AsyncGrpcClient
from google.protobuf.empty_pb2 import Empty

from ._utils import catch_errors, filter_dict
from .constants import ServiceStatusCode
from .schema import (
    About,
    Server,
    ServerDiagnostics,
    ServerFailure,
    ServerInfo,
    ServiceStatusInfo,
)

_logger = logging.getLogger(__name__)


class DiagnosticsDecoder(GrpcDecoder):
    """Diagnostics service decoder."""

    decode_unset_messages = False
    decode_empty_string_none = False

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(StatusReply.Data.Service, self._decode_service)
        self.set_enum_decoder(StatusReply.Data.Service.Status, ServiceStatusCode)

    def _decode_service(self, value: StatusReply.Data.Service) -> ServiceStatusInfo:
        """Decode service status information."""
        info = self._decode_message(value)
        return ServiceStatusInfo(**info)


class DiagnosticsClient(AsyncGrpcClient):
    """Diagnostics service gRPC client."""

    service = "Diagnostics"
    decoder_cls = DiagnosticsDecoder
    stub_cls = DiagnosticsStub

    @catch_errors
    async def about(self) -> About:
        """Return information about server and API versions.

        Raises:
            DiagnosticsNotImplemented: If diagnostics service is not implemented
                on the server.
            ServiceUnavailable: If connection to the service failed.
            DiagnosticsError: If there has been any other problem with the response.
        """
        result = await self.call("about", Empty())
        return About(**result or {})

    @catch_errors
    async def status(self) -> Dict[str, ServiceStatusInfo]:
        """Return service status information for all available services.

        Raises:
            DiagnosticsNotImplemented: If diagnostics service is not implemented
                on the server.
            ServiceUnavailable: If connection to the service failed.
            DiagnosticsError: If there has been any other problem with the response.
        """
        result = await self.call("status", Empty())
        return cast(Dict[str, ServiceStatusInfo], result or {})


async def collect_server_diagnostics(servers: Iterable[Server]) -> ServerDiagnostics:
    """Collect version and service status information from a group of servers."""
    clients = [
        DiagnosticsClient(
            server.netloc, credentials=server.credentials, ssl_cert=server.ssl_cert
        )  # type: ignore[call-overload] # treated using Server.check_credentials
        for server in servers
    ]
    tasks = [asyncio.gather(client.about(), client.status()) for client in clients]
    results = await asyncio.gather(*tasks, return_exceptions=True)

    diag = ServerDiagnostics()
    for server, result in zip(servers, results):
        if isinstance(result, BaseException):
            _logger.warning(
                "Could not get diagnostics from '%s'.", server.netloc, exc_info=result
            )
            diag.failures.append(
                ServerFailure(
                    netloc=server.netloc,
                    type=result.__class__.__name__,
                    message=str(result.__context__),
                )
            )
            continue

        about_result, status_result = result
        server_info = ServerInfo(
            netloc=server.netloc,
            server_version=about_result.server_version,
            api_versions=filter_dict(about_result.api_versions, server.services),
            services=filter_dict(status_result, server.services),
        )
        diag.servers.append(server_info)
    return diag
