"""Diagnostic gRPC services."""

from types import ModuleType
from typing import Any, Callable, Dict, List

import fred_api.diagnostics.service_diagnostics_grpc_pb2_grpc as diagnostics_grpc
import grpc
from fred_api.diagnostics import service_diagnostics_grpc_pb2
from fred_api.diagnostics.service_diagnostics_grpc_pb2 import AboutReply, StatusReply
from frgal.service import wrap_unary
from google.protobuf.descriptor import FileDescriptor

from .constants import ServiceStatusCode
from .schema import ServiceStatusInfo

try:
    from importlib_metadata import version as package_version  # Python < 3.10
except ImportError:  # pragma: no cover
    from importlib.metadata import version as package_version


def _diag_not_implemented() -> ServiceStatusInfo:
    return ServiceStatusInfo(
        status=ServiceStatusCode.UNKNOWN, note="Diagnostics not implemented."
    )


class DiagnosticsServicer(diagnostics_grpc.DiagnosticsServicer):
    """Diagnostics gRPC service."""

    def __init__(self, server_version: str, register_self: bool = True) -> None:
        """Initialize the diagnostics service.

        Args:
            server_version: Server version reported in the About reply.
            register_self:
                If True, register DiagnosticsServicer to be reported in replies.
        """
        self.server_version = server_version
        self.api_versions: Dict[str, str] = {}
        self.services: Dict[str, Callable[[], ServiceStatusInfo]] = {}
        if register_self:
            service_name = self.add_grpc_api(service_diagnostics_grpc_pb2)[0]
            self.add_service_status(
                service_name, lambda: ServiceStatusInfo(status=ServiceStatusCode.OK)
            )

    def add_api(self, api_name: str, api_version: str) -> None:
        """Add an API manually to be included in AboutReply."""
        self.api_versions[api_name] = api_version

    def add_grpc_api(self, service_module: ModuleType) -> List[str]:
        """Include the given gRPC API in replies to clients.

        The gRPC service from the given module is registered.

        AboutReply will list the newly registered service in `api_versions`
        together with the version of the module's package (retrieved using `importlib`).

        StatusReply will list the newly registered service with an UNKNOWN status.
        (Use the `add_service_status` method to provide actual status information.)

        Args:
            service_module:
                A `service_*_grpc_pb2` module,
                e.g. `fred_api.diagnostics.service_diagnostics_grpc_pb2`.

        Returns:
            Names of registered APIs.
        """
        if service_module.__package__ is None:
            raise ValueError("Module must be part of a package to determine version.")
        version = package_version(service_module.__package__)
        descriptor: FileDescriptor = service_module.DESCRIPTOR
        registered_apis = []
        for service in descriptor.services_by_name:
            full_service_name = f"{descriptor.package}.{service}"
            self.add_api(full_service_name, version)
            registered_apis.append(full_service_name)
            if full_service_name not in self.services:
                self.add_service_status(full_service_name, _diag_not_implemented)
        return registered_apis

    def add_service_status(
        self, service_name: str, status_callback: Callable[[], ServiceStatusInfo]
    ) -> None:
        """Add a service to be included in StatusReply."""
        self.services[service_name] = status_callback

    def add_to_server(self, server: grpc.Server) -> None:
        """Add DiagnosticsServicer to the given gRPC server."""
        diagnostics_grpc.add_DiagnosticsServicer_to_server(self, server)

    @wrap_unary
    def about(self, request: Any, context: Any) -> AboutReply:
        """Return server version and versions of registered APIs."""
        reply = AboutReply()
        reply.data.server_version.value = self.server_version
        for name, version in self.api_versions.items():
            reply.data.api_versions[name].value = version
        return reply

    @wrap_unary
    def status(self, request: Any, context: Any) -> StatusReply:
        """Return statuses of registered services."""
        reply = StatusReply()
        for service_name, status_callback in self.services.items():
            service_status = status_callback()
            service_reply = reply.data.services[service_name]
            service_reply.status = service_status.status
            service_reply.note.value = service_status.note
            for extra_name, extra_value in service_status.extras.items():
                service_reply.extras[extra_name].value = extra_value
        return reply
