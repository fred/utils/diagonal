from typing import Sequence, Tuple
from unittest import TestCase

from grpc import ChannelCredentials

from diagonal.constants import ServiceStatusCode, StatusCode
from diagonal.schema import (
    Server,
    ServerDiagnostics,
    ServerFailure,
    ServerInfo,
    ServiceStatusInfo,
)


def get_server_info(**kwargs: dict) -> ServerInfo:
    defaults: dict = {
        "netloc": "example.com",
        "server_version": "1.0.0",
        "api_versions": {},
        "services": {},
    }
    defaults.update(kwargs)
    return ServerInfo(**defaults)


class TestServer(TestCase):
    def test_credentials(self):
        srv = Server(netloc="example.com", credentials=ChannelCredentials(None))

        self.assertIsNotNone(srv.credentials)
        self.assertIsNone(srv.ssl_cert)

    def test_ssl_cert(self):
        srv = Server(netloc="example.com", ssl_cert="cert.pem")

        self.assertIsNone(srv.credentials)
        self.assertIsNotNone(srv.ssl_cert)

    def test_credentials_and_ssl_cert(self):
        with self.assertRaises(ValueError):
            Server(
                netloc="example.com",
                credentials=ChannelCredentials(None),
                ssl_cert="cert.pem",
            )


class TestServerInfo(TestCase):
    def test_summary_status(self):
        data: Sequence[Tuple[dict, StatusCode]] = (
            ({}, StatusCode.OK),
            ({"s1": ServiceStatusInfo(status=ServiceStatusCode.OK)}, StatusCode.OK),
            (
                {"s1": ServiceStatusInfo(status=ServiceStatusCode.UNKNOWN)},
                StatusCode.WARNING,
            ),
            (
                {
                    "s1": ServiceStatusInfo(status=ServiceStatusCode.WARNING),
                    "s2": ServiceStatusInfo(status=ServiceStatusCode.OK),
                },
                StatusCode.WARNING,
            ),
            (
                {
                    "s1": ServiceStatusInfo(status=ServiceStatusCode.OK),
                    "s2": ServiceStatusInfo(status=ServiceStatusCode.ERROR),
                    "s3": ServiceStatusInfo(status=ServiceStatusCode.WARNING),
                    "s4": ServiceStatusInfo(status=ServiceStatusCode.UNKNOWN),
                },
                StatusCode.ERROR,
            ),
        )

        for services, summary_status in data:
            with self.subTest(services=services, summary_status=summary_status):
                srv_info = get_server_info(services=services)
                self.assertEqual(srv_info.summary_status, summary_status)


class TestServerDiagnostics(TestCase):
    def test_summary_status_no_servers_no_failures(self):
        self.assertEqual(ServerDiagnostics().summary_status, StatusCode.OK)

    def test_summary_status_no_servers_failure(self):
        diag = ServerDiagnostics(
            failures=[
                ServerFailure(netloc="example.com", type="type", message="message")
            ]
        )

        self.assertEqual(diag.summary_status, StatusCode.ERROR)

    def test_summary_status_max_ok(self):
        diag = ServerDiagnostics(
            servers=[
                get_server_info(
                    services={"s1": ServiceStatusInfo(status=ServiceStatusCode.OK)}
                ),
                get_server_info(
                    services={"s2": ServiceStatusInfo(status=ServiceStatusCode.OK)}
                ),
            ]
        )

        self.assertEqual(diag.summary_status, StatusCode.OK)

    def test_summary_status_max_warning(self):
        diag = ServerDiagnostics(
            servers=[
                get_server_info(
                    services={"s1": ServiceStatusInfo(status=ServiceStatusCode.OK)}
                ),
                get_server_info(
                    services={"s2": ServiceStatusInfo(status=ServiceStatusCode.WARNING)}
                ),
                get_server_info(
                    services={"s3": ServiceStatusInfo(status=ServiceStatusCode.OK)}
                ),
            ]
        )
        self.assertEqual(diag.summary_status, StatusCode.WARNING)

    def test_summary_status_failures_and_ok_servers(self):
        diag = ServerDiagnostics(
            failures=[
                ServerFailure(netloc="example.com", type="type", message="message")
            ],
            servers=[
                get_server_info(
                    services={"s1": ServiceStatusInfo(status=ServiceStatusCode.OK)}
                ),
                get_server_info(
                    services={"s2": ServiceStatusInfo(status=ServiceStatusCode.OK)}
                ),
            ],
        )
        self.assertEqual(diag.summary_status, StatusCode.ERROR)
