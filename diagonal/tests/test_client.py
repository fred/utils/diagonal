from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import patch, sentinel

from fred_api.diagnostics.service_diagnostics_grpc_pb2 import AboutReply, StatusReply
from frgal.utils import AsyncTestClientMixin
from google.protobuf.empty_pb2 import Empty
from grpc import RpcError, StatusCode

from diagonal.client import (
    About,
    DiagnosticsClient,
    DiagnosticsDecoder,
    collect_server_diagnostics,
)
from diagonal.constants import ServiceStatusCode
from diagonal.exceptions import (
    DiagnosticsError,
    DiagnosticsNotImplemented,
    ServiceUnavailable,
)
from diagonal.schema import Server, ServerFailure, ServerInfo, ServiceStatusInfo


class TestDiagnosticsClient(AsyncTestClientMixin, DiagnosticsClient):
    """Test DiagnosticsClient."""


class DiagnosticsDecoderTest(TestCase):
    def setUp(self):
        self.decoder = DiagnosticsDecoder()

    def test_decode_service(self):
        service = StatusReply.Data.Service()
        service.status = 2
        service.note.value = "note"
        service.extras["x"].value = "X"
        service.extras["y"].value = "Y"
        self.assertEqual(
            self.decoder.decode(service),
            ServiceStatusInfo(
                status=ServiceStatusCode.WARNING,
                note="note",
                extras={"x": "X", "y": "Y"},
            ),
        )


class DiagnosticsClientTest(IsolatedAsyncioTestCase):
    """Test DiagnosticsClient's methods."""

    def setUp(self):
        self.client = TestDiagnosticsClient(netloc=sentinel.netloc)

    async def test_about(self):
        about = AboutReply()
        about.data.server_version.value = "1.0.0"
        about.data.api_versions["Diagnostics"].value = "0.1.0"
        about.data.api_versions["AnotherApi"].value = "0.0.1"
        self.client.mock.return_value = about

        self.assertEqual(
            await self.client.about(),
            About(
                server_version="1.0.0",
                api_versions={"Diagnostics": "0.1.0", "AnotherApi": "0.0.1"},
            ),
        )

        self.client.mock.assert_called_once_with(
            Empty(),
            method="/Fred.Diagnostics.Api.Diagnostics/about",
            timeout=None,
        )

    async def test_status(self):
        status = StatusReply()
        status.data.services["Service"].status = 3
        status.data.services["Service"].note.value = "note"
        status.data.services["Service"].extras["extra"].value = "value"
        status.data.services["Diagnostics"].status = 1
        status.data.services["Diagnostics"].note.value = ""
        self.client.mock.return_value = status

        self.assertEqual(
            await self.client.status(),
            {
                "Service": ServiceStatusInfo(
                    status=ServiceStatusCode.ERROR,
                    note="note",
                    extras={"extra": "value"},
                ),
                "Diagnostics": ServiceStatusInfo(status=ServiceStatusCode.OK),
            },
        )

        self.client.mock.assert_called_once_with(
            Empty(),
            method="/Fred.Diagnostics.Api.Diagnostics/status",
            timeout=None,
        )

    async def _test_grpc_error(self, method):
        data = (
            (StatusCode.UNAVAILABLE, ServiceUnavailable),
            (StatusCode.UNIMPLEMENTED, DiagnosticsNotImplemented),
            (StatusCode.UNKNOWN, DiagnosticsError),
        )

        for status, exc in data:
            with self.subTest(status=status, exc=exc):
                error = RpcError()
                error.code = lambda: status  # noqa: B023
                self.client.mock.side_effect = error
                with self.assertRaises(exc):
                    await method()

    async def test_about_error(self):
        await self._test_grpc_error(self.client.about)

    async def test_status_error(self):
        await self._test_grpc_error(self.client.status)

    async def test_about_empty(self):
        self.client.mock.return_value = AboutReply()

        with self.assertRaises(DiagnosticsError):
            await self.client.about()

        self.client.mock.assert_called_once_with(
            Empty(),
            method="/Fred.Diagnostics.Api.Diagnostics/about",
            timeout=None,
        )

    async def test_status_empty(self):
        self.client.mock.return_value = StatusReply()

        self.assertEqual(await self.client.status(), {})

        self.client.mock.assert_called_once_with(
            Empty(),
            method="/Fred.Diagnostics.Api.Diagnostics/status",
            timeout=None,
        )


def diagnostics_error_from(original_exception: Exception) -> DiagnosticsError:
    """Exception like produced by `raise DiagnosticsError from original_exception`."""
    exception = DiagnosticsError()
    exception.__context__ = original_exception
    return exception


@patch("diagonal.client.DiagnosticsClient.status")
@patch("diagonal.client.DiagnosticsClient.about")
class CollectServerDiagnosticsTest(IsolatedAsyncioTestCase):
    def setUp(self):
        self.servers = [
            Server(netloc="server1"),
            Server(netloc="server2"),
            Server(netloc="server3"),
        ]

    async def test_single_about_fail(self, about_mock, status_mock):
        about_mock.side_effect = [
            About(server_version="1.0.0", api_versions={"api_1": "1.1.1"}),
            diagnostics_error_from(RpcError("foo")),
            About(server_version="1.2.3", api_versions={"api_3": "1.3.5"}),
        ]
        status_mock.side_effect = [
            {"service1": ServiceStatusInfo(status=ServiceStatusCode.OK)},
            {"service2": ServiceStatusInfo(status=ServiceStatusCode.OK)},
            {"service3": ServiceStatusInfo(status=ServiceStatusCode.OK)},
        ]

        diag = await collect_server_diagnostics(self.servers)

        self.assertEqual(
            diag.failures,
            [ServerFailure(netloc="server2", type="DiagnosticsError", message="foo")],
        )
        self.assertEqual(
            diag.servers,
            [
                ServerInfo(
                    netloc="server1",
                    server_version="1.0.0",
                    api_versions={"api_1": "1.1.1"},
                    services={
                        "service1": ServiceStatusInfo(status=ServiceStatusCode.OK)
                    },
                ),
                ServerInfo(
                    netloc="server3",
                    server_version="1.2.3",
                    api_versions={"api_3": "1.3.5"},
                    services={
                        "service3": ServiceStatusInfo(status=ServiceStatusCode.OK)
                    },
                ),
            ],
        )
        self.assertEqual(diag.summary_status, ServiceStatusCode.ERROR)

    async def test_single_status_fail(self, about_mock, status_mock):
        about_mock.side_effect = [
            About(server_version="1.0.0", api_versions={"api_1": "1.1.1"}),
            About(server_version="1.0.1", api_versions={"api_2": "1.2.4"}),
            About(server_version="1.2.3", api_versions={"api_3": "1.3.5"}),
        ]
        status_mock.side_effect = [
            diagnostics_error_from(RpcError("bar")),
            {"service2": ServiceStatusInfo(status=ServiceStatusCode.OK)},
            {"service3": ServiceStatusInfo(status=ServiceStatusCode.WARNING)},
        ]

        diag = await collect_server_diagnostics(self.servers)

        self.assertEqual(
            diag.failures,
            [ServerFailure(netloc="server1", type="DiagnosticsError", message="bar")],
        )
        self.assertEqual(
            diag.servers,
            [
                ServerInfo(
                    netloc="server2",
                    server_version="1.0.1",
                    api_versions={"api_2": "1.2.4"},
                    services={
                        "service2": ServiceStatusInfo(status=ServiceStatusCode.OK)
                    },
                ),
                ServerInfo(
                    netloc="server3",
                    server_version="1.2.3",
                    api_versions={"api_3": "1.3.5"},
                    services={
                        "service3": ServiceStatusInfo(status=ServiceStatusCode.WARNING)
                    },
                ),
            ],
        )
        self.assertEqual(diag.summary_status, ServiceStatusCode.ERROR)
