from typing import Sequence, Tuple
from unittest import TestCase

from diagonal import _utils
from diagonal.constants import StatusCode


class TestUtils(TestCase):
    def test_filter_dict(self):
        data: Sequence[Tuple[dict, Sequence, dict]] = (
            ({"a": 1, "b": 2, "c": 3}, ("a", "c"), {"a": 1, "c": 3}),
            ({"a": 1, "b": 2, "c": 3}, [], {"a": 1, "b": 2, "c": 3}),
            ({"a": "a", "b": "b"}, ("a", "b", "c"), {"a": "a", "b": "b"}),
            ({1: 2, 2: 3, 3: 4}, (1, 4), {1: 2}),
            ({}, ["a", "b"], {}),
            ({}, (), {}),
        )

        for input, keys_to_keep, result in data:
            with self.subTest(input=input, keys_to_keep=keys_to_keep, result=result):
                filtered_data = _utils.filter_dict(input, keys_to_keep)
                self.assertEqual(filtered_data, result)

    def test_aggregate_status_code(self):
        data: Sequence[Tuple[Sequence[StatusCode], StatusCode]] = (
            ((StatusCode.WARNING, StatusCode.ERROR, StatusCode.OK), StatusCode.ERROR),
            ((StatusCode.WARNING, StatusCode.WARNING), StatusCode.WARNING),
            ((StatusCode.WARNING, StatusCode.ERROR), StatusCode.ERROR),
            ((StatusCode.WARNING, StatusCode.OK), StatusCode.WARNING),
            ((StatusCode.ERROR, StatusCode.WARNING), StatusCode.ERROR),
            ((StatusCode.ERROR, StatusCode.ERROR), StatusCode.ERROR),
            ((StatusCode.OK, StatusCode.OK), StatusCode.OK),
            ([StatusCode.OK], StatusCode.OK),
            ((), StatusCode.OK),
        )

        for statuses, result in data:
            with self.subTest(statuses=statuses, result=result):
                aggregate_status = _utils.aggregate_status_code(statuses)
                self.assertEqual(aggregate_status, result)
