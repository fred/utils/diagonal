from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel

from fred_api.diagnostics.service_diagnostics_grpc_pb2 import AboutReply, StatusReply

from diagonal.constants import ServiceStatusCode
from diagonal.schema import ServiceStatusInfo
from diagonal.service import DiagnosticsServicer


class DiagnosticsServicerTest(TestCase):
    @patch("diagonal.service.service_diagnostics_grpc_pb2.DESCRIPTOR")
    def test_init_with_register_self(self, descriptor_mock):
        descriptor_mock.package = "pkg"
        descriptor_mock.services_by_name = ["service"]

        servicer = DiagnosticsServicer(sentinel.version, register_self=True)

        self.assertEqual(servicer.server_version, sentinel.version)
        self.assertIn("pkg.service", servicer.api_versions)
        self.assertIn("pkg.service", servicer.services)

    def test_init_without_register_self(self):
        servicer = DiagnosticsServicer(sentinel.version, register_self=False)

        self.assertEqual(servicer.server_version, sentinel.version)
        self.assertFalse(servicer.api_versions)
        self.assertFalse(servicer.services)

    def test_add_grpc_api_module_without_package(self):
        servicer = DiagnosticsServicer("1.0.0")
        module_mock = Mock()
        module_mock.__package__ = None

        with self.assertRaises(ValueError):
            servicer.add_grpc_api(module_mock)

    @patch("diagonal.service.package_version")
    def test_add_grpc_api_service_registered_already(self, package_version_mock):
        package_version_mock.return_value = "1.2.3"
        api = Mock()
        api.__package__ = Mock()
        api.DESCRIPTOR.package = "pkg"
        api.DESCRIPTOR.services_by_name = ["service"]
        servicer = DiagnosticsServicer("1.0.0", register_self=False)
        servicer.add_service_status("pkg.service", sentinel.status_callback)

        apis = servicer.add_grpc_api(api)

        self.assertEqual(apis, ["pkg.service"])
        self.assertEqual(servicer.api_versions, {"pkg.service": "1.2.3"})
        self.assertEqual(servicer.services, {"pkg.service": sentinel.status_callback})

    @patch("diagonal.service.diagnostics_grpc.add_DiagnosticsServicer_to_server")
    def test_add_to_server(self, add_servicer_to_server_mock):
        servicer = DiagnosticsServicer("1.0.0")
        servicer.add_to_server(sentinel.server)

        add_servicer_to_server_mock.assert_called_once_with(servicer, sentinel.server)

    @patch("diagonal.service.service_diagnostics_grpc_pb2.DESCRIPTOR")
    @patch("diagonal.service.package_version")
    def test_about(self, package_version_mock, descriptor_mock):
        package_version_mock.return_value = "1.2.3"
        descriptor_mock.package = "pkg"
        descriptor_mock.services_by_name = ["service"]
        servicer = DiagnosticsServicer("1.0.0")

        about_reply: AboutReply = servicer.about(Mock(), None)

        self.assertEqual(about_reply.data.server_version.value, "1.0.0")
        self.assertEqual(len(about_reply.data.api_versions), 1)
        self.assertIn("pkg.service", about_reply.data.api_versions)
        self.assertEqual(about_reply.data.api_versions["pkg.service"].value, "1.2.3")

    def test_status(self):
        servicer = DiagnosticsServicer("1.0.0", register_self=False)
        status_callback = Mock()
        status_callback.return_value = ServiceStatusInfo(
            status=ServiceStatusCode.WARNING, note="foo", extras={"bar": "baz"}
        )
        servicer.add_service_status("Service", status_callback)

        status_reply: StatusReply = servicer.status(Mock(), None)

        self.assertEqual(status_callback.mock_calls, [call()])
        self.assertEqual(len(status_reply.data.services), 1)
        self.assertIn("Service", status_reply.data.services)
        service: StatusReply.Data.Service = status_reply.data.services["Service"]
        self.assertEqual(service.status, StatusReply.Data.Service.Status.warning)
        self.assertEqual(service.note.value, "foo")
        self.assertEqual(len(service.extras), 1)
        self.assertIn("bar", service.extras)
        self.assertEqual(service.extras["bar"].value, "baz")

    @patch("diagonal.service.package_version")
    def test_status_default_reply_grpc_api(self, package_version_mock):
        api = Mock()
        api.__package__ = Mock()
        api.DESCRIPTOR.package = "pkg"
        api.DESCRIPTOR.services_by_name = ["service"]
        servicer = DiagnosticsServicer("1.0.0", register_self=False)
        servicer.add_grpc_api(api)

        status_reply: StatusReply = servicer.status(Mock(), None)

        self.assertEqual(len(status_reply.data.services), 1)
        self.assertIn("pkg.service", status_reply.data.services)
        service: StatusReply.Data.Service = status_reply.data.services["pkg.service"]
        self.assertEqual(service.status, StatusReply.Data.Service.Status.unknown)
        self.assertEqual(service.note.value, "Diagnostics not implemented.")
        self.assertFalse(service.extras)
