"""Diagonal - a client library for Fred service diagnostics."""

from .client import About, DiagnosticsClient, collect_server_diagnostics
from .constants import ServiceStatusCode, StatusCode
from .exceptions import DiagnosticsError, DiagnosticsNotImplemented, ServiceUnavailable
from .schema import ServiceStatusInfo
from .service import DiagnosticsServicer

__version__ = "3.0.0"

__all__ = [
    "About",
    "DiagnosticsClient",
    "DiagnosticsError",
    "DiagnosticsNotImplemented",
    "DiagnosticsServicer",
    "ServiceStatusCode",
    "ServiceStatusInfo",
    "ServiceUnavailable",
    "StatusCode",
    "collect_server_diagnostics",
]
