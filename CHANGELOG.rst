ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

3.0.0 (2025-03-05)
------------------

* Add ``collect_server_diagnostics`` function (#12)
* Add ``DiagnosticsServicer`` (#13)
* Breaking changes (#12)

  * Move ``client.About`` to ``schema.About``
  * Move and rename

    * ``client.Service`` to ``schema.ServiceStatusInfo``
    * ``client.Status`` to ``constants.ServiceStatusCode``

  * Remove ``DiagnosticsDecoder.STATUS_CODE_ENUM``

* Upgrade to fred-frgal ~= 4.1
* Drop Python 3.8 support

2.0.0 (2024-09-23)
------------------

* Upgrade to pydantic 2 (#11)

1.1.1 (2024-09-23)
------------------

* Upgrade to fred-frgal ~= 3.15 (#10)

1.1.0 (2024-06-24)
------------------

* Add Pydantic v1/v2 compatibility layer
* Add specific service exceptions

  * ``DiagnosticsNotImplemented`` when server does not provide diagnostics service
  * ``ServiceUnavailable`` when service is not available at all

1.0.0 (2022-12-21)
------------------

* Update project setup
* Bump version to 1.0.0

0.2.0 (2022-05-16)
------------------

* Use pydantic models
* Reformat code with Black

0.1.1 (2021-11-02)
------------------

* Expose DiagnosticsClient in __init__.py

0.1.0 (2021-10-18)
------------------

Initial version.
